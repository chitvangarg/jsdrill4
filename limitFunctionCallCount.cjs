const limitFunctionCallCount = function (cb, n){

    if (typeof cb !== 'function') {
        throw new Error('The first parameter must be a function');
      }
      
      if (typeof n !== 'number' || n <= 0) {
        throw new Error('The second parameter must be a positive number');
      }

    let counter = 0;
    return function(){
        if(counter < n){
            counter++;
            return cb.apply(null, arguments);
        }
        else{
            return null;
        }        
    }
}

module.exports = limitFunctionCallCount
