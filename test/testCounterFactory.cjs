const counterFactory = require('../counterFactory.cjs');

const firstFactory = new counterFactory();

// first factory

console.log(firstFactory.increment())
console.log(firstFactory.increment())
console.log(firstFactory.increment())
console.log(firstFactory.increment())

console.log(firstFactory.decrement())
console.log(firstFactory.decrement())
console.log(firstFactory.decrement())

// second factory

const secondFactory = new counterFactory();

console.log(secondFactory.increment())
console.log(secondFactory.increment())
console.log(secondFactory.increment())

console.log(secondFactory.decrement())
console.log(secondFactory.decrement())
console.log(secondFactory.decrement())