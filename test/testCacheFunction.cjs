const cacheFunction = require('../cacheFunction.cjs')

function callback(...args){
    return JSON.stringify(args);
}


try{
    let cache = cacheFunction(callback);
    console.log(cache(10,20,30))
    console.log(cache(10,20,30))
}
catch(err){
    console.error(err.message)
}