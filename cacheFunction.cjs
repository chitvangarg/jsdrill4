const cacheFunction = (cb) => {
    let cache = {};

    if(typeof cb !== 'function'){
        throw new Error("Partial parameters not allowed");
    }

    return function(...args){

        // if(args.length === 0){
        //     throw new Error("No parameters provides")
        // }
        
        const key = JSON.stringify(args);

        if(key in cache){
            return cache[key];
        }
        
        let result = cb(...args);

        cache[key] = result;

        return result;
    }
}

module.exports = cacheFunction;

